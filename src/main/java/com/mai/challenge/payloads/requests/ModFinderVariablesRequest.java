package com.mai.challenge.payloads.requests;

import lombok.AllArgsConstructor;
import lombok.Setter;
import lombok.Getter;

@Getter
@Setter
@AllArgsConstructor
public class ModFinderVariablesRequest {

    /**
     * x variable in mod finder calculation.
     */
    private Integer x;
    /**
     * y variable in mod finder calculation.
     */
    private Integer y;
    /**
     * n variable in mod finder calculation.
     */
    private Integer n;
}
