package com.mai.challenge.payloads.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Response of Maximum mod operation finder operation.
 */
@AllArgsConstructor
@Getter
@Setter
public class MaximumModOperationResponse {

    /**
     * X variable used for computing the response.
     */
    private Integer x;

    /**
     * Y variable used for computing the response.
     */
    private Integer y;

    /**
     * N variable used for computing the response.
     */
    private Integer n;

    /**
     * Maximum mod found in the operation execution.
     */
    private Integer maximumK;
}
