package com.mai.challenge;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages = "com.mai.challenge.*")
@OpenAPIDefinition(info= @Info(title = "Derek Mai's Inclusion Cloud challenge.", version = "1.0",
		description = "This is the resolution of Inclusion Cloud challenge given to Derek Mai."))
public class ChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChallengeApplication.class, args);
	}

}
