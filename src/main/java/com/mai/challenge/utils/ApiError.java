package com.mai.challenge.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Generic api error POJO.
 */
@AllArgsConstructor
@Getter
@Setter
public class ApiError {

    /**
     * Http status code.
     */
    private int code;
    /**
     * Human readable error message.
     */
    private String errorMessage;
}
