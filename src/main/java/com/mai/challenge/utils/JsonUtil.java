package com.mai.challenge.utils;


import com.google.gson.Gson;

/**
 * Json parsing utility.
 */
public class JsonUtil {

    private static final Gson gson = new Gson();


    public String convertPayload(Object payload) {
        return gson.toJson(payload);
    }
}
