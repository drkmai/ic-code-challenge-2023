package com.mai.challenge.services;

import com.mai.challenge.commands.MaximumModFinderCommand;
import com.mai.challenge.payloads.requests.ModFinderVariablesRequest;
import com.mai.challenge.payloads.responses.MaximumModOperationResponse;
import org.springframework.stereotype.Service;


/**
 * Maximum Mod Finder service.
 */
@Service
public class ModFinderService {

    private MaximumModOperationResponse lastSuccessfulOperation = null;

    public MaximumModOperationResponse executeOperationWithGivenVariables(ModFinderVariablesRequest
                                                                                  numericVariablesRequest) {

        MaximumModFinderCommand command = new MaximumModFinderCommand(numericVariablesRequest.getX(),
                numericVariablesRequest.getY(), numericVariablesRequest.getN());
        Integer maximumMod = command.execute();

        MaximumModOperationResponse result = new MaximumModOperationResponse(numericVariablesRequest.getX(), numericVariablesRequest.getY(),
                numericVariablesRequest.getN(), maximumMod);
        lastSuccessfulOperation = result;
        return result;
    }

    public MaximumModOperationResponse getLastSuccessfulOperation() {
        return lastSuccessfulOperation ;
    }
}
