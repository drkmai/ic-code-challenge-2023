package com.mai.challenge.exceptions.impl;

/**
 * Missing required value exception custom exception.
 */
public class MissingRequiredValueException extends RuntimeException {
    public MissingRequiredValueException(String errorMessage) {
        super(errorMessage);
    }
}
