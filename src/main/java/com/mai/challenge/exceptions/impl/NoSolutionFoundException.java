package com.mai.challenge.exceptions.impl;

public class NoSolutionFoundException  extends RuntimeException   {
    public NoSolutionFoundException(Integer x, Integer y, Integer n) {
        super(String.format("No solution found with given values. x: %s, y: %s, n: %s",x,y,n));
    }
}
