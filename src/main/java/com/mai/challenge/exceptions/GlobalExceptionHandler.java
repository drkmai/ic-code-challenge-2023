package com.mai.challenge.exceptions;

import com.mai.challenge.exceptions.impl.MissingRequiredValueException;
import com.mai.challenge.exceptions.impl.NoSolutionFoundException;
import com.mai.challenge.utils.ApiError;
import com.mai.challenge.utils.JsonUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;



/**
 * Global exception handling.
 */
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {MissingRequiredValueException.class, IllegalArgumentException.class,
            NoSolutionFoundException.class})
    @ResponseBody
    public ResponseEntity<Object> handleBadRequest(Exception e) {
        JsonUtil jsonUtil = new JsonUtil();
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST.value(), e.getMessage());
        System.out.println(jsonUtil.convertPayload(apiError));
        return new ResponseEntity<>(jsonUtil.convertPayload(apiError),HttpStatus.BAD_REQUEST);
    }
}
