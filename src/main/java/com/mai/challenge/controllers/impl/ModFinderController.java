package com.mai.challenge.controllers.impl;

import com.mai.challenge.controllers.ModFinderApi;
import com.mai.challenge.payloads.requests.ModFinderVariablesRequest;
import com.mai.challenge.payloads.responses.MaximumModOperationResponse;
import com.mai.challenge.services.ModFinderService;
import com.mai.challenge.utils.ApiError;
import com.mai.challenge.utils.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;


@RestController
@Slf4j
public class ModFinderController implements ModFinderApi {

    private final ModFinderService modFinderService;

    private final JsonUtil jsonUtil = new JsonUtil();

    /**
     * Constructor.
     *
     * @param modFinderService service that will prove mod finder logic methods.
     */
    public ModFinderController(ModFinderService modFinderService) {
        this.modFinderService = modFinderService;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<String> getLastExecutionData() {
        MaximumModOperationResponse response = modFinderService.getLastSuccessfulOperation();
        return new ResponseEntity<>(Objects.nonNull(response) ? jsonUtil.convertPayload(response) :
                jsonUtil.convertPayload(new ApiError(200, "There's no operation to fetch.")), HttpStatus.OK);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<String> executeCalculationWithGivenNumbers(@RequestBody ModFinderVariablesRequest
                                                                             modFinderVariablesRequest) {
        MaximumModOperationResponse asd = modFinderService
                .executeOperationWithGivenVariables(modFinderVariablesRequest);
        System.out.println(asd);
        String response = jsonUtil.convertPayload(modFinderService
                .executeOperationWithGivenVariables(modFinderVariablesRequest));
        System.out.println(response);
        return new ResponseEntity<>(jsonUtil.convertPayload(modFinderService
                .executeOperationWithGivenVariables(modFinderVariablesRequest)), HttpStatus.OK);
    }
}
