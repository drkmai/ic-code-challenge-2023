package com.mai.challenge.controllers;

import com.mai.challenge.payloads.requests.ModFinderVariablesRequest;
import com.mai.challenge.payloads.responses.MaximumModOperationResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Maximum mod finder endpoints.
 */
@Tag(name = "1. Maximum Mod Finder", description = "Endpoints related to finding maximum mod.")
@RequestMapping("${spring.data.rest.base-path}/mod")
public interface ModFinderApi {


    @Operation(summary = "Provides (x,y,n) and maximum k from last execution if there's any.",
            tags = {"1. Maximum Mod Finder"})
    @ApiResponse(responseCode = "200",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = MaximumModOperationResponse.class)))
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<String> getLastExecutionData();

    @Operation(summary = "Given (x,y,n), provides the Maximum 'k'. (x,y,n) must be greater or equal than 0.",
            tags = {"1. Maximum Mod Finder"})
    @ApiResponse(responseCode = "200",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = MaximumModOperationResponse.class)))
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<String> executeCalculationWithGivenNumbers(ModFinderVariablesRequest modFinderVariablesRequest);
}
