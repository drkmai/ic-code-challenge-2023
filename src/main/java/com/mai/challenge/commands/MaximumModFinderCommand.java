package com.mai.challenge.commands;

import com.mai.challenge.exceptions.impl.MissingRequiredValueException;
import com.mai.challenge.exceptions.impl.NoSolutionFoundException;

import java.util.Objects;


public class MaximumModFinderCommand {

    private Integer x;
    private Integer y;
    private Integer n;


    public MaximumModFinderCommand(Integer x,
                                   Integer y,
                                   Integer n) {
        this.setX(x);
        this.setY(y);
        this.setN(n);
    }

    private void setX(Integer x) {
        if (Objects.isNull(x)) {
            throw new MissingRequiredValueException("X variable can't be null.");
        }
        if (x < 0L) {
            throw new IllegalArgumentException("X has to be greater or equal than 0.");
        }
        this.x = x;
    }

    private void setY(Integer y) {
        if (Objects.isNull(y)) {
            throw new MissingRequiredValueException("Y variable can't be null.");
        }
        if (y < 0L) {
            throw new IllegalArgumentException("Y has to be greater or equal than 0.");
        }
        this.y = y;
    }

    private void setN(Integer n) {
        if (Objects.isNull(n)) {
            throw new MissingRequiredValueException("N variable can't be null.");
        }
        if (n < 0L) {
            throw new IllegalArgumentException("N has to be greater or equal than 0.");
        }
        this.n = n;
    }

    public Integer execute() {
        int maximumValue = Integer.MIN_VALUE;
        if (x== 0) {
            throw new NoSolutionFoundException(getX(),getY(),getN());
        }
        for (int k = 0; k <= n; k++) {
            if (k % x == y) {
                maximumValue = Math.max(maximumValue, k);
            }
        }
        if (maximumValue >= 0) {
            return maximumValue;
        }
        throw new NoSolutionFoundException(getX(),getY(),getN());
    }

    /**
     * Getters
     */
    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }

    public Integer getN() {
        return n;
    }
}
