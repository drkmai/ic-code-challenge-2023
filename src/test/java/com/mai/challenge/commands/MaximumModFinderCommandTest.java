package com.mai.challenge.commands;

import com.mai.challenge.exceptions.impl.MissingRequiredValueException;
import com.mai.challenge.exceptions.impl.NoSolutionFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MaximumModFinderCommandTest {

    private MaximumModFinderCommand command;

    private final String INVALID_VALUE_ERROR_MESSAGE = "%s has to be greater or equal than 0.";

    private final String NULL_VALUE_ERROR_MESSAGE = "%s variable can't be null.";

    private final String NO_SOLUTION_ERROR_MESSAGE = "No solution found with given values. x: %s, y: %s, n: %s";

    @BeforeEach
    public void cleanMaximumModFinder() {
        command = null;
    }

    @Test()
    public void when_classContructorReceivesValidInput_isValid() {
        MaximumModFinderCommand command = new MaximumModFinderCommand(1, 2, 3);
        assertTrue(command.getX().equals(1));
        assertTrue(command.getY().equals(2));
        assertTrue(command.getN().equals(3));
    }

    @Test()
    public void when_classContructorReceivesInvalidX_isInvalid() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new MaximumModFinderCommand(-1, 2, 3));
        String expectedErrorMessage = String.format(INVALID_VALUE_ERROR_MESSAGE, "X");
        String actualErrorMessage = exception.getMessage();
        assertTrue(actualErrorMessage.contains(expectedErrorMessage));
    }

    @Test()
    public void when_classContructorReceivesInvalidY_isInvalid() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new MaximumModFinderCommand(1, -1, 3));
        String expectedErrorMessage = String.format(INVALID_VALUE_ERROR_MESSAGE, "Y");
        String actualErrorMessage = exception.getMessage();
        assertTrue(actualErrorMessage.contains(expectedErrorMessage));
    }

    @Test()
    public void when_classContructorReceivesInvalidN_isInvalid() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new MaximumModFinderCommand(1, 2, -1));
        String expectedErrorMessage = String.format(INVALID_VALUE_ERROR_MESSAGE, "N");
        String actualErrorMessage = exception.getMessage();
        assertTrue(actualErrorMessage.contains(expectedErrorMessage));
    }

    @Test()
    public void when_classContructorReceivesNullX_isInvalid() {
        Exception exception = assertThrows(MissingRequiredValueException.class, () -> new MaximumModFinderCommand(null, 2, 3));
        String expectedErrorMessage = String.format(NULL_VALUE_ERROR_MESSAGE, "X");
        String actualErrorMessage = exception.getMessage();
        assertTrue(actualErrorMessage.contains(expectedErrorMessage));
    }

    @Test()
    public void when_classContructorReceivesNullY_isInvalid() {
        Exception exception = assertThrows(MissingRequiredValueException.class, () -> new MaximumModFinderCommand(1, null, 3));
        String expectedErrorMessage = String.format(NULL_VALUE_ERROR_MESSAGE, "Y");
        String actualErrorMessage = exception.getMessage();
        assertTrue(actualErrorMessage.contains(expectedErrorMessage));
    }

    @Test()
    public void when_classContructorReceivesNullN_isInvalid() {
        Exception exception = assertThrows(MissingRequiredValueException.class, () -> new MaximumModFinderCommand(1, 2, null));
        String expectedErrorMessage = String.format(NULL_VALUE_ERROR_MESSAGE, "N");
        String actualErrorMessage = exception.getMessage();
        assertTrue(actualErrorMessage.contains(expectedErrorMessage));
    }

    @Test()
    public void when_execute_hasNoSolution_throwsException() {

        command = new MaximumModFinderCommand(1, 1, 3);
        Exception exception = assertThrows(NoSolutionFoundException.class, () -> command.execute());
        String expectedErrorMessage = String.format(NO_SOLUTION_ERROR_MESSAGE,1,1,3);
        String actualErrorMessage = exception.getMessage();
        assertTrue(actualErrorMessage.contains(expectedErrorMessage));
    }

    @Test()
    public void when_execute_hasSolution_1() {

        command = new MaximumModFinderCommand(7, 5, 12345);
        Integer expectedResult = 12339;
        Integer actualResult = command.execute();
        assertTrue(actualResult.equals(expectedResult));
    }

    @Test()
    public void when_execute_hasSolution_2() {

        command = new MaximumModFinderCommand(5, 0, 4);
        Integer expectedResult = 0;
        Integer actualResult = command.execute();
        assertTrue(actualResult.equals(expectedResult));
    }
    @Test()
    public void when_execute_hasSolution_3() {

        command = new MaximumModFinderCommand(10, 5, 15);
        Integer expectedResult = 15;
        Integer actualResult = command.execute();
        assertTrue(actualResult.equals(expectedResult));
    }
    @Test()
    public void when_execute_hasSolution_4() {

        command = new MaximumModFinderCommand(17, 8, 54321);
        Integer expectedResult = 54306;
        Integer actualResult = command.execute();
        assertTrue(actualResult.equals(expectedResult));
    }
    @Test()
    public void when_execute_hasSolution_5() {

        command = new MaximumModFinderCommand(499999993, 9, 1000000000);
        Integer expectedResult = 999999995;
        Integer actualResult = command.execute();
        assertTrue(actualResult.equals(expectedResult));
    }
    @Test()
    public void when_execute_hasSolution_6() {

        command = new MaximumModFinderCommand(10, 5, 187);
        Integer expectedResult = 185;
        Integer actualResult = command.execute();
        assertTrue(actualResult.equals(expectedResult));
    }
    @Test()
    public void when_execute_hasSolution_7() {

        command = new MaximumModFinderCommand(2, 0, 999999999);
        Integer expectedResult = 999999998;
        Integer actualResult = command.execute();
        assertTrue(actualResult.equals(expectedResult));
    }


}
