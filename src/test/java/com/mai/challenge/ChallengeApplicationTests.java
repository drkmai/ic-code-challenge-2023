package com.mai.challenge;

import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.Suite;

@SelectPackages({"com.mai.challenge.commands", "com.mai.challenge.controllers"})
@Suite
class ChallengeApplicationTests {
}
