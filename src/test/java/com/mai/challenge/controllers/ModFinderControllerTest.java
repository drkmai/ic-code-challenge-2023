package com.mai.challenge.controllers;

import com.google.gson.Gson;
import com.mai.challenge.controllers.impl.ModFinderController;
import com.mai.challenge.payloads.requests.ModFinderVariablesRequest;
import com.mai.challenge.payloads.responses.MaximumModOperationResponse;
import com.mai.challenge.services.ModFinderService;
import com.mai.challenge.utils.JsonUtil;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ModFinderController.class)
public class ModFinderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ModFinderService service;

    @Mock
    private JsonUtil jsonUtil;

    private ModFinderVariablesRequest modFinderRequest = new ModFinderVariablesRequest(7,5,12345);

    private MaximumModOperationResponse modResponse = new MaximumModOperationResponse(7,5,12345,12339);

    @Value("${spring.data.rest.base-path}")
    private String basePath;

    @Test
    public void contextLoads(){}

    @Test
    public void shouldReturn_FirstSolutionMessage() throws Exception {
        when(service.getLastSuccessfulOperation()).thenReturn(null);
        String modUrl = "/".concat(basePath.concat("/mod"));
        this.mockMvc.perform(get(modUrl)).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("There\\u0027s no operation to fetch.")));
    }

    @Test
    public void shouldReturn_maximumK() throws Exception {
        Gson gson = new Gson();
        when(service.executeOperationWithGivenVariables(any())).thenReturn(modResponse);
        when(jsonUtil.convertPayload(modResponse)).thenReturn(gson.toJson(modResponse));
        String modUrl = "/".concat(basePath.concat("/mod"));
        this.mockMvc
                .perform(post(modUrl)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(modFinderRequest)))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("\"x\":7")))
                .andExpect(content().string(containsString("\"y\":5")))
                .andExpect(content().string(containsString("\"n\":12345")))
                .andExpect(content().string(containsString("\"maximumK\":12339")));
    }

    @Test
    public void shouldReturn_lastResult() throws Exception {
        Gson gson = new Gson();
        when(service.getLastSuccessfulOperation()).thenReturn(modResponse);
        when(jsonUtil.convertPayload(any())).thenReturn(gson.toJson(modResponse));
        String modUrl = "/".concat(basePath.concat("/mod"));

        this.mockMvc.perform(get(modUrl)).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("\"x\":7")))
                .andExpect(content().string(containsString("\"y\":5")))
                .andExpect(content().string(containsString("\"n\":12345")))
                .andExpect(content().string(containsString("\"maximumK\":12339")));
    }
}
