# Derek Mai's  Inclussion Cloud Code Challenge



## Getting started

There's no need to configure  anything. Application is running in port 8080. In http://localhost:8080/swagger-ui/index.html a Swagger UI that explains how to consume the API.

## Development Process

* 1° I developed the algorithm asked and made unit test in order to check that it functions correctly.
* 2° I developed the API in order to consume the algorithm as requested.
* 3° Generated the Swagger to document it's use.
* 4° I Wrote the Test Suit case.


## Dependencies used

* Spring Boot 2.7.4
* Spring boot maven plugin
* Spring Doc 1.6.12
* Spring web
* Spring test
* Gson 2.8.9
* Junit platform suit engine  1.8.1
* Project Lombok
